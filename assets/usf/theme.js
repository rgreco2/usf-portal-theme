/**
 * Javascript for the USF System Academics Portal
 * Author: Robert Greco (rgreco2@usf.edu)
 * Version 1.0
 */

//@koala-prepend '../js-cookie-2.1.3/js.cookie.js'

var CLOSE_BTN = '<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>';

$(function() {
    $('.dismissable').each(function() {
        var $this = $(this),
            $close;
        $this.css('position', 'relative');
        $close = $(CLOSE_BTN).css('position','absolute')
                            .css('top', 10)
                            .css('right', 10)
                            .click(function() {
                                $this.slideUp();
                                if(typeof $this.data('remember') !== typeof undefined)
                                    Cookies.set('catms-close-' + $this.data('remember'), true);
                            });

        $this.append($close);
    });
});
